<?php

namespace Drupal\webform_google_map_element\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'webform_google_map_element' element.
 *
 * @WebformElement(
 *   id = "webform_google_map_element",
 *   label = @Translation("Google Embedded Map"),
 *   description = @Translation("Provides a webform element map."),
 *   category = @Translation("Map elements"),
 * )
 *
 * @see \Drupal\webform_google_map_element\Element\WebformGoogleMapElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformGoogleMapElement extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'multiple' => '',
      'size' => '',
      'minlength' => '',
      'maxlength' => '',
      'placeholder' => '',
      'lat' => '',
      'lon' => '',
      'zoom' => '',
      'zoomclick' => '',
      'view' => '',
      'nopopup' => '',
      'emptytitle' => '',
      'notopic' => '',
      'filterundefined' => '',
      'filterempty' => '',
      'selectoption' => [],
      'compid' => '',
      'selecttitle' => '',
    ] + parent::defineDefaultProperties();

  }

  /**
   * {@inheritdoc}
   */
  protected function defineTranslatableProperties() {
    return array_merge(parent::defineTranslatableProperties(), [
      'selecttitle',
      'selectoption',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['element']['webform_google_map'] = [
      '#type' => 'details',
      '#title' => t('Map Setting'),
    ];
    $form['element']['webform_google_map']['lat'] = [
      '#type' => 'textfield',
      '#title' => t('Lattitude'),
      '#required' => TRUE,
    ];
    $form['element']['webform_google_map']['lon'] = [
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#required' => TRUE,
    ];
    $form['element']['webform_google_map']['zoom'] = [
      '#type' => 'textfield',
      '#title' => t('Zoom'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#description' => t('Choose a value between 14-18 for the zoom. You can try out different values and see which one fits better to show the area on the map that you want to show.'),
    ];
    $form['element']['webform_google_map']['zoomclick'] = [
      '#type' => 'textfield',
      '#title' => t('Zoom after click'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => t('When someone places a point, the map will zoom in to this level. Typically a higher number than the zoom of the map.'),
    ];

    $form['element']['webform_google_map']['view'] = [
      '#type' => 'select',
      '#title' => t('View'),
      '#options' => [
        'hybrid' => t('HYBRID'),
        'roadmap' => t('ROADMAP'),
        'satellite' => t('SATELLITE'),
        'terrain' => t('TERRAIN'),
      ],
    ];
    $form['element']['webform_google_map']['compid'] = [
      '#type' => 'textfield',
      '#title' => t('Component number of additional information'),
      '#size' => 60,
      '#maxlength' => 128,
      '#description' => t('the answers to the components number added in this field will be added to the table with points on the map reportult.'),
    ];
    $form['element']['webform_google_map']['nopopup'] = [
      '#type' => 'checkbox',
      '#title' => t('No popup?'),
    ];
    $form['element']['webform_google_map']['emptytitle'] = [
      '#title' => t('Empty Label'),
      '#type' => 'textfield',
    ];
    $form['element']['webform_google_map']['notopic'] = [
      '#type' => 'checkbox',
      '#title' => t('Popup with only comment'),
    ];
    $form['element']['webform_google_map']['filterundefined'] = [
      '#type' => 'checkbox',
      '#title' => t('Filter undefined points'),
    ];
    $form['element']['webform_google_map']['filterempty'] = [
      '#type' => 'checkbox',
      '#title' => t('Filter Empty points'),
    ];

    $form['element']['webform_google_map']['selecttitle'] = [
      '#title' => t('Select type of point'),
      '#type' => 'textfield',
    ];

    $form['element']['webform_google_map']['selectoption'] = [
      '#title' => $this->t('Markers'),
      '#type' => 'webform_google_map_element_markers',
    ];

    return $form;
  }

}

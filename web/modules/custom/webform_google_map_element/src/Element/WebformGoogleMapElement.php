<?php

namespace Drupal\webform_google_map_element\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Provides a 'webform_google_map_element' form element.
 *
 * @FormElement("webform_google_map_element")
 */
class WebformGoogleMapElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#process' => [
        [$class, 'processWebformGoogleMapElement'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformGoogleMapElement'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformGoogleMapElement'],
      ],
      '#theme' => 'detail__webform_example_element',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Implement your logic here if needed.
  }

  /**
   * Processes a 'webform_google_map_element' element.
   */
  public static function processWebformGoogleMapElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $map_js_settings = [];
    $element['#tree'] = TRUE;
    $element['#value'] = (!is_array($element['#value'])) ? [] : $element['#value'];

    $element['webformgoogleimage'] = [
      '#type' => 'item',
      '#markup' => '
        <div id="type-selector" class="controls"></div>
        <div class="mapwrapertoppopup">' . t('Complete the data in the form on the left and save.') . '
          <div class="mapwrapertopin btn btn-primary">' . t('Close') . '</div>
        </div>
        <div id="' . $element['#name'] . '" class="mapwebform"></div>',
      '#translatable' => ['title', 'description'],
    ];

    $key_parents = [];
    foreach ($element['#parents'] as $parent) {
      $key_parents[] = $parent;
    }

    $element['webformgoogledata'] = [
      '#title' => t('Data And Coordinates'),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#description' => t('Click on the map to fill the form. The data will be shown in this textarea as a JSON format.'),
      '#attributes' => [
        'class' => ['hidetextarea1', 'datafield', 'imagedata'],
        'id' => [$element['#name'] . '_latlonclassresult'],
      ],
      '#default_value' => (isset($element['#default_value']) && !is_array($element['#default_value'])) ? $element['#default_value'] : '',
      '#weight' => $element['#weight'] + 0.1,
      '#prefix' => '<div class="visually-hidden">',
      '#suffix' => '</div>',
    ];

    $map_js_settings[$element['#name']] = [
      'lat' => isset($element['#lat']) ? $element['#lat'] : '',
      'lon' => isset($element['#lon']) ? $element['#lon'] : '',
      'zoom' => isset($element['#zoom']) ? $element['#zoom'] : '',
      'zoomclick' => isset($element['#zoomclick']) ? $element['#zoomclick'] : '',
      'view' => isset($element['#view']) ? $element['#view'] : '',
      'nopopup' => isset($element['#nopopup']) ? $element['#nopopup'] : '',
      'emptytitle' => isset($element['#emptytitle']) ? $element['#emptytitle'] : '',
      'notopic' => isset($element['#notopic']) ? $element['#notopic'] : '',
      'filterundefined' => isset($element['#filterundefined']) ? $element['#filterundefined'] : '',
      'filterempty' => isset($element['#filterempty']) ? $element['#filterempty'] : '',
      'selectoption' => isset($element['#selectoption']) ? WebformGoogleMapElement::getMarkerurl($element['#selectoption']) : '',
      'selecttitle' => $element['#selecttitle'] ?? '',
      'save_label' => t('Save'),
      'delete_label' => t('Delete'),
    ];

    $element['#attached']['library'][] = 'webform_google_map_element/webform_google_map_element';
    $element['#attached']['drupalSettings']['webform_google_map_element']['webform_google_map_element'] = $map_js_settings;

    // Pass '#value__*' properties to the value element.
    foreach ($element as $key => $value) {
      if (strpos($key, '#value__') === 0) {
        $value_key = str_replace('#value__', '#', $key);
        $element['value'][$value_key] = $value;
      }
    }

    // Pass the entire element to the value element.
    if (isset($element['#element'])) {
      $element['value'] += $element['#element'];
    }

    if (!empty($element['#states'])) {
      WebformFormHelper::processStates($element, '#wrapper_attributes');
    }

    return $element;
  }

  /**
   * Webform element validation handler for 'webform_google_map_element'.
   */
  public static function validateWebformGoogleMapElement(&$element, FormStateInterface $form_state, &$complete_form) {
    // Your existing validation logic here.
    $form_state->setValueForElement($element, $element['webformgoogledata']['#value']);
  }

  /**
   * Prepares a 'webform_google_map_element' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformGoogleMapElement(array $element) {
    if (empty($element['#lat']) && empty($element['#lon']) && empty($element['#zoom'])) {
      \Drupal::messenger()->addMessage(t('Before submitting this form, go to the current webform map element config page and fill the required fields.'), MessengerInterface::TYPE_WARNING);
    }
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, ['id', 'name', 'value']);
    static::setAttributes($element, ['form-text', 'webform-map-element']);
    return $element;
  }

  /**
   * Gets marker URLs for the select option.
   *
   * @param array $selectoption
   *   The select option array.
   *
   * @return array
   *   The array of select options.
   */
  protected static function getMarkerurl(array $selectoption) {
    // Your existing logic here.
    $selectoptions = [];
    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    foreach ($selectoption as $option_key => $option) {
      $media = Media::load($option['src']);
      $fid = $media->getSource()->getSourceFieldValue($media);
      $file = File::load($fid);
      $url = $base_url . $file->createFileUrl();
      $selectoptions[] = [
        $option_key,
        $option['text'],
        $url,
      ];
    }
    return $selectoptions;
  }

}

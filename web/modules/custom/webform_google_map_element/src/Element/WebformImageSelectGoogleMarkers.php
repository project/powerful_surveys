<?php

namespace Drupal\webform_google_map_element\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Utility\WebformElementHelper;

/**
 * Provides a form element to assist in creation of webform select image images.
 *
 * @FormElement("webform_google_map_element_markers")
 */
class WebformImageSelectGoogleMarkers extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#label' => $this->t('Marker'),
      '#labels' => $this->t('Markers'),
      '#min_items' => 3,
      '#empty_items' => 1,
      '#add_more_items' => 1,
      '#process' => [
        [$class, 'processWebformImageSelectGoogleMarkers'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      if (!isset($element['#default_value'])) {
        return [];
      }
      return $element['#default_value'];
    }
    elseif (is_array($input) && isset($input['images'])) {
      return $input['images'];
    }
    else {
      return NULL;
    }
  }

  /**
   * Process images and build images widget.
   */
  public static function processWebformImageSelectGoogleMarkers(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#tree'] = TRUE;

    // Add validate callback that extracts the associative array of images.
    $element['#element_validate'] = [
      [get_called_class(), 'validateWebformImageSelectImages'],
    ];

    // Wrap this $element in a <div> that handle #states.
    WebformElementHelper::fixStatesWrapper($element);

    $properties = [
      '#label',
      '#labels',
      '#min_items',
      '#empty_items',
      '#add_more_items',
    ];

    $element['images'] = array_intersect_key($element, array_combine($properties, $properties)) + [
      '#type' => 'webform_multiple',
      '#key' => 'value',
      '#header' => [
        ['data' => t('Image value'), 'width' => '25%'],
        ['data' => t('Image text'), 'width' => '25%'],
        ['data' => t('Image src'), 'width' => '50%'],
      ],
      '#element' => [
        'value' => [
          '#type' => 'textfield',
          '#title' => t('Image value'),
          '#title_display' => 'invisible',
          '#placeholder' => t('Enter value…'),
          '#error_no_message' => TRUE,
          '#attributes' => ['class' => ['js-webform-options-sync']],
        ],
        'text' => [
          '#type' => 'textfield',
          '#title' => t('Image text'),
          '#title_display' => 'invisible',
          '#placeholder' => t('Enter text…'),
          '#error_no_message' => TRUE,
        ],
        'src' => [
          '#type' => 'media_library',
          '#allowed_bundles' => ['image'],
          '#title' => t('Upload your image'),
          '#description' => t('Upload or select your profile image.'),
          '#multiple' => FALSE,
        ],
      ],
      '#error_no_message' => TRUE,
      '#add_more_input_label' => t('more images'),
      '#default_value' => $element['#default_value'] ?? [],
    ];

    $element['#attached']['library'][] = 'webform/webform.element.options.admin';

    return $element;
  }

  /**
   * Validates webform image select images element.
   */
  public static function validateWebformImageSelectImages(&$element, FormStateInterface $form_state, &$complete_form) {
    $options = NestedArray::getValue($form_state->getValues(), $element['images']['#parents']);

    // Validate required images.
    if (!empty($element['#required']) && empty($options)) {
      WebformElementHelper::setRequiredError($element, $form_state);
      return;
    }

    $form_state->setValueForElement($element, $options);
  }

}

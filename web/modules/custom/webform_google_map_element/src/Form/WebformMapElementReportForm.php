<?php

namespace Drupal\webform_google_map_element\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Render\Markup;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Defines the form for the Webform Map Element Report.
 */
class WebformMapElementReportForm extends FormBase {

  /**
   * The Webform ID.
   *
   * @var string
   */
  protected $webformId;

  /**
   * The map iframe content.
   *
   * @var string
   */
  protected $mapIframe;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_map_report_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $webform = NULL) {

    if ($webform == NULL) {
      throw new \Exception('Webform ID not found.');
    }

    $this->webformId = $webform;
    $webid = $this->webformId;
    $webform = \Drupal::entityTypeManager()->getStorage('webform')->load($webid);
    $elements = $webform->getElementsDecodedAndFlattened();
    $options_question_type[] = $this->t('Select');
    foreach ($elements as $element_key => $element) {
      $type = $element['#type'];
      if ($type == 'webform_google_map_element' || $type == 'webform_google_map_element_route11') {
        $title = $element['#title'];
        $options_question_type[$element_key] = $title;
      }
    }

    $form_state->setMethod('POST');

    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this
        ->t('FILTERS'),
    ];

    $form['filters']['reportcid'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Question'),
      '#options' => $options_question_type,
      '#default_value' => isset($_GET['reportcid']) ? [$_GET['reportcid']] : [],
    ];
    $form['filters']['keyword'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Keyword'),
      '#default_value' => isset($_GET['keyword']) ? [$_GET['keyword']] : '',
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['filters']['topic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('topic'),
      '#default_value' => isset($_GET['topic']) ? [$_GET['topic']] : '',
      '#size' => 60,
      '#maxlength' => 128,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['show_map'] = [
      '#type' => 'submit',
      '#value' => $this->t('Show Code'),
      '#ajax' => [
        'callback' => '::showCode',
        'event' => 'click',
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh Results'),
    ];

    $form['actions']['rsubmit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply Filter'),
    ];

    $typea = $this->checkElementPresense($webid);
    $newrecords = $this->getSubmissionsData($webid, $typea);
    $centerpoint = $this->getCenterPoint($webid);
    $apikey = $this->generateRandomApiKey();

    if (empty($centerpoint['lat']) && empty($centerpoint['long']) && empty($centerpoint['zoom'])) {
      $form['fetched_data_display'] = [
        '#markup' => Markup::create('Please fill the map centerpoint details under Setting tab Form Tab Custom google map setting.'),
      ];
    }

    elseif (empty($typea)) {
      $form['fetched_data_display'] = [
        '#markup' => Markup::create('There are no google map element on this form'),
      ];
    }

    elseif (empty($newrecords)) {
      $form['fetched_data_display'] = [
        '#markup' => Markup::create('Please create a submission with map element and add a point on the map'),
      ];
    }

    // Render the fetched data if available.
    else {
      if (!empty($form_state->get('fetched_data'))) {
        $form['fetched_data_display'] = [
          '#markup' => Markup::create($form_state->get('fetched_data')),
        ];
      }
      else {
        $feched_data = $this->getRefreshedResults($newrecords, $apikey, $centerpoint);
        $this->mapIframe = $feched_data[1];
        $form['fetched_data_display'] = [
          '#markup' => Markup::create($feched_data[0]),
        ];
      }
    }

    $form['#attached']['library'][] = 'webform_google_map_element/webform_google_map_element';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getIconUrl($record, $webform) {
    $element = $webform->getElement($record->name);
    $base_url = \Drupal::request()->getSchemeAndHttpHost();
    foreach (json_decode($record->value) as $key => $value) {
      if (in_array($value->Select_Machine, array_keys($element['#selectoption']))) {
        ;
      }{
      $media = Media::load($element['#selectoption'][$value->Select_Machine]['src'][0]);
      $fid = $media->getSource()->getSourceFieldValue($media);
      $file = File::load($fid);
      $url = $base_url . $file->createFileUrl();
      ;
      $value->iconurl = $url;
      $newValue[] = $value;
      }
    }
    $record->value = json_encode($newValue);
    return $record;
  }

  /**
   * {@inheritdoc}
   */
  public function showCode() {
    $response = new AjaxResponse();

    // If you want to further prevent Drupal from rendering as HTML:
    $escapedText = new FormattableMarkup('<code><pre>@text</pre></code>', ['@text' => $this->mapIframe]);

    $options = [
    // Add any additional options you need.
      'dialogClass' => 'show_map_code',
      'Class' => 'show_map_code',
    ];
    $response->addCommand(new OpenModalDialogCommand('Copy Code', $escapedText, $options));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshedResults($newrecords, $apikey, $centerpoint) {
    $request = [
      $newrecords,
      $apikey,
      $_POST,
      $centerpoint,
    ];
    // Instantiate the HTTP client.
    $httpClient = \Drupal::httpClient();

    // Specify the API endpoint URL.
    $apiUrl = 'http://api.createlli.com/create-map-reportings/map-reportings-data?_format=json';

    // Prepare the request body.
    $requestBody = json_encode($request);
    // Send the POST request to the API.
    try {
      $response = $httpClient->post($apiUrl, [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'body' => $requestBody,
      ]);
      // Get the response status code.
      $statusCode = $response->getStatusCode();
      // Process the API response as needed.
      if ($statusCode == 200) {
        // API call successful.
        $responseData = explode("/n", $response->getBody()->getContents());

        return $responseData;
      }
    }
    catch (\Exception $e) {
      // Handle the exception.
      \Drupal::logger('webform_google_map_element')->error($e->getMessage());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function deleteMapReportings(string $webid, string $apikey, array $sids) {
    $request = [
      $webid,
      $apikey,
      $sids,
    ];
    // Instantiate the HTTP client.
    $httpClient = \Drupal::httpClient();

    // Specify the API endpoint URL.
    $apiUrl = 'http://api-ctm.arushinfotech.com/create-map-reportings/map-reportings-data/delete-submission?_format=json';

    // Prepare the request body.
    $requestBody = json_encode($request);
    // Send the POST request to the API.
    try {
      $response = $httpClient->post($apiUrl, [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'body' => $requestBody,
      ]);
      // Get the response status code.
      $statusCode = $response->getStatusCode();
      // Process the API response as needed.
      if ($statusCode == 200) {
        // API call successful.
        return $response;
      }
    }
    catch (\Exception $e) {
      // Handle the exception.
      \Drupal::logger('webform_google_map_element')->error($e->getMessage());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function insertMapReportings($newrecords, $apikey) {
    $request = [
      $newrecords,
      $apikey,
    ];

    // Instantiate the HTTP client.
    $httpClient = \Drupal::httpClient();
    $requestBody = json_encode($request);
    // Specify the API endpoint URL.
    $apiUrl = 'http://api-ctm.arushinfotech.com/create-map-reportings/map-reportings-data/insert-submission?_format=json';

    try {
      $response = $httpClient->post($apiUrl, [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'body' => $requestBody,
      ]);
      // Get the response status code.
      $statusCode = $response->getStatusCode();
      // Process the API response as needed.
      if ($statusCode == 200) {

        return $response;
      }
    }
    catch (\Exception $e) {
      // Handle the exception.
      \Drupal::logger('webform_google_map_element')->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $webid = $this->webformId;
    $apikey = $this->generateRandomApiKey();
    $typea = $this->checkElementPresense($webid);
    $newrecords = $this->getSubmissionsData($webid, $typea);
    $centerpoint = $this->getCenterPoint($webid);
    $feched_data = $this->getRefreshedResults($newrecords, $apikey, $centerpoint);
    $this->mapIframe = $feched_data[1];
    // Display the HTML content on the form.
    $form_state->set('fetched_data', $feched_data[0]);
    // Display the HTML content on the form.
    $form_state->setRebuild();
    \Drupal::messenger()->addMessage(t("Records updated successfully"));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmissionsData($webid, $typea, $sid = NULL) {
    $webform = \Drupal::entityTypeManager()->getStorage('webform')->load($webid);
    $database = \Drupal::database();
    $query = "SELECT * FROM webform_submission_data wsd inner join webform_submission ws on ws.sid = wsd.sid where wsd.webform_id = '$webid' and wsd.name IN ($typea) and wsd.value != ''";
    if ($sid != NULL) {
      $query .= "and wsd.sid = $sid";
    }
    $records = $database->query($query)->fetchAll();
    $newrecords = [];
    foreach ($records as $record) {
      $newrecords[] = $this->getIconUrl($record, $webform);
    }

    return $newrecords;
  }

  /**
   * Generates a random API key.
   *
   * @return string
   *   The generated API key.
   */
  public function generateRandomApiKey() {
    $query = \Drupal::database()->select('webform_google_map_element_api', 'cmea')
      ->fields('cmea', ['key'])->execute()->fetchAll();
    if (!empty($query)) {
      $apikey = $query[0]->key;
    }
    else {
      // Define the length of the API key.
      // You can adjust this length as needed.
      $length = 32;

      // Generate random bytes.
      $randomBytes = random_bytes($length);

      // Convert the random bytes to a hexadecimal string.
      $hashedApiKey = bin2hex($randomBytes);

      // Hash the API key to improve security.
      $apikey = hash('sha256', $hashedApiKey);
      $result = \Drupal::database()->insert('webform_google_map_element_api')
        ->fields([
          'key' => $apikey,
        ])
        ->execute();
    }

    return $apikey;
  }

  /**
   * {@inheritdoc}
   */
  public function getCenterPoint($webid) {
    $webform = \Drupal::entityTypeManager()->getStorage('webform')->load($webid);
    $centerpoint = [
      "lat" => $webform->getThirdPartySetting('webform_google_map_element', 'latitude1'),
      "long" => $webform->getThirdPartySetting('webform_google_map_element', 'longitude1'),
      "zoom" => $webform->getThirdPartySetting('webform_google_map_element', 'zoom1'),
      "map_type" => $webform->getThirdPartySetting('webform_google_map_element', 'map_type1'),
    ];
    return $centerpoint;
  }

  /**
   * {@inheritdoc}
   */
  public function checkElementPresense($webid) {
    $webform = \Drupal::entityTypeManager()->getStorage('webform')->load($webid);
    $elements = $webform->getElementsDecodedAndFlattened();
    $typea = [];
    foreach ($elements as $element_key => $element) {
      $type = $element['#type'];
      if ($type == 'webform_google_map_element') {
        $typea[] = "'$element_key'";
      }
    }
    $typea = implode(',', $typea);

    return $typea;
  }

}

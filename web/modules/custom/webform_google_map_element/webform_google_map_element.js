(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.googleMapsCRUD = {
    attach: function (context, settings) { 
      const maps={}; 
      let map;
      let isInfoWindowOpen = []; 
      const markerDataArray = {};
      let option_array = [];
      let elementdata = drupalSettings.webform_google_map_element.webform_google_map_element;
      const markers = {};
      const infowindows = {};
      function initMap(element) {
        map = new google.maps.Map(document.getElementById(element), {
          center: { lat: parseFloat(elementdata[element].lat), lng: parseFloat(elementdata[element].lon) },
          zoom: parseFloat(elementdata[element].zoom),
          mapTypeId: elementdata[element].view,
        });
        isInfoWindowOpen[element]=false;
        var inputElement = $('<input type="text" class="addressclass" id="' + element + '_addressclass">');
        var input = inputElement[0];

        // Initialize Google Maps Places Autocomplete with the raw DOM element
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        autocomplete.addListener('place_changed', function() {
          const place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          }
          if(!isInfoWindowOpen[element]){
            if (!isDuplicateMarker(place.geometry.location,element)) {
              const marker = createMarker(place.geometry.location,element);
              openInfoWindow(marker,element);
              reverseGeocode(marker,element);
            } else {
              window.alert('Duplicate marker found. Cannot add the same location.');
            }  
          }
          else{
            window.alert('Please complete the current info window form.');
          }
          // Rest of your code for handling selected place remains unchanged...
        });
        map.addListener('fullscreen_changed', function() {
          if (map.fullscreen) {
            // Map entered fullscreen mode
            console.log('Map entered fullscreen mode');
            // Add your code to execute when entering fullscreen mode
          } else {
            // Map exited fullscreen mode
            console.log('Map exited fullscreen mode');
            // Add your code to execute when exiting fullscreen mode
          }
        });
        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
        maps[element]=map;
        markers[element]=[];
        infowindows[element]=[];
        markerDataArray[element]=[];
        map.addListener('click', function (event) {          
          // Check for duplicate marker
          if(!isInfoWindowOpen[element]){
            if (!isDuplicateMarker(event.latLng,element)) {
              const marker = createMarker(event.latLng,element);
              openInfoWindow(marker,element);
              reverseGeocode(marker,element);
            } else {
              alert('Duplicate marker found. Cannot add the same location.');
            }  
          }
          else{
            alert('Please complete the current info window form.');
          }
        });
        if(drupalSettings.webform_google_map_element.webform_google_map_element.nopopup !== 1) {
            //alert(Drupal.t('Complete the data in the form on the left and save.'));
            $( ".mapwrapertoppopup" ).addClass( "mapwrapertoppopupshow" );
        }
      }
      
      function openInfoWindow(marker,element) {
        const index = markers[element].indexOf(marker);
        if (index >= 0 && index < infowindows[element].length) {
          infowindows[element][index].open(map, marker);
          isInfoWindowOpen[element] = true;
        } else {
          console.error("Marker or infowindow not found.");
        }
      }
      document.onfullscreenchange = function ( event ) {
        let target = event.target;
        let pacContainerElements = document.getElementsByClassName("pac-container");
        if (pacContainerElements.length > 0) {
          let pacContainer = document.querySelector('.pac-container:last-child');
          if (pacContainer.parentElement === target) {
            document.getElementsByTagName("body")[0].appendChild(pacContainer);
            pacContainer.className += pacContainer.className.replace("fullscreen-pac-container", "");
          } else {
            target.appendChild(pacContainer);
            pacContainer.className += " fullscreen-pac-container";
          }
        }
      };
      function isDuplicateMarker(newLocation,element) {
        // Compare new location with existing markers
        for (let i = 0; i < markers[element].length; i++) {
          const existingLocation = markers[element][i].getPosition();
          if (newLocation.equals(existingLocation)) {
            return true; // Duplicate marker found
          }
        }
        return false; // No duplicate marker found
      }

      function createMarker(location,element) {
        const marker = new google.maps.Marker({
          position: location,
          map: maps[element],
          draggable: true,
          label: {
            text: markers[element].length.toString(), // Display the marker index as the label
            color: 'white', // Label text color
            fontWeight: 'bold', // Label text style
          }
        });
        option_array = getoptiionsarray(element);
        const infowindow = new google.maps.InfoWindow({
          content: `
            <div class='wrap-imgpoint-texta' id=${element+'_inflowindow'}>
              <input type="text" id="formattedAddress"><br>
              <select id="selectOption" class="imgpoint-selecta">
                ${option_array}
              </select><br>
              <textarea id="textarea"></textarea><br>
              <button onclick="saveMarker(${markers[element].length},'${element}')" type="button" name="map-save">${elementdata[element].save_label}</button>
              <button onclick="deleteMarker(${markers[element].length},'${element}')">${elementdata[element].delete_label}</button>
            </div>
          `
        });

        marker.addListener('dblclick', function () {
          const index = markers[element].indexOf(marker);
          if (index !== -1 && confirm("Do you want to delete this point?")) {
            deleteMarker(index,element);
          }
        });

        marker.addListener('click', function () {
          openInfoWindow(marker,element);
        });

        marker.addListener('dragend', function () {
          reverseGeocode(marker,element);
          openInfoWindow(marker,element);
        });

        markers[element].push(marker);
        infowindows[element].push(infowindow);
        isInfoWindowOpen[element] = true;

        // Close the info window when it's closed
        infowindow.addListener('closeclick', function () {
          const index = markers[element].indexOf(marker);
          if(markerDataArray[element][index] == undefined){
            if (index !== -1) {
              deleteMarker(index,element);
            }
          }
          isInfoWindowOpen[element] = false;
        });

        return marker;
      }

      function reverseGeocode(marker,element) {
        const geocoder = new google.maps.Geocoder();
        const index = markers[element].indexOf(marker);
    
        if (index !== -1 && index < infowindows[element].length) {
          const position = marker.getPosition();
    
          geocoder.geocode({ location: position }, function (results, status) {
            if (status === "OK" && results[0]) {
              const formattedAddress = results[0].formatted_address;
              $('#'+element+'_inflowindow').children('#formattedAddress').val(formattedAddress);
              document.getElementById(element+'_addressclass').value = formattedAddress;
            } else {
              console.error("Geocoder failed with status: " + status);
            }
          });
        }
      }

      function saveMarker(index,element) {
        if (index >= 0 && index < markers[element].length) {
          const formattedAddress = $('#'+element+'_inflowindow').children('#formattedAddress').val();
          const selectOption = $('#'+element+'_inflowindow').children('#selectOption').val();
          const selectOptionhtml = $('#'+element+'_inflowindow').children('#selectOption').children('option:selected').text();
          const textarea = $('#'+element+'_inflowindow').children('textarea').val();
          if(selectOption.trim()==''){            
            alert(Drupal.t('Please select a topic'));
            return false;
          }
          // Get the marker's position (latitude and longitude)
          const markerPosition = markers[element][index].getPosition().toJSON();
          
          let iconurl;
          for(i=0;i<elementdata[element].selectoption.length;i++){
            if(elementdata[element].selectoption[i][0]==selectOption){
              iconurl=elementdata[element].selectoption[i][2];
              const markerPosition = markers[element][index].getPosition().toJSON();
              markers[element][index].setIcon( /** @type {google.maps.Icon} */({
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35),
                url:iconurl,
              }));
              break;
            }
          }
          if(markerDataArray[element][index] !== ''){
            markerDataArray[element][index]={
              "Point": index+1,
              "Latitude": markerPosition.lat,
              "Longitude": markerPosition.lng,
              "Address": formattedAddress,
              "Select_Machine": selectOption,
              "Select_Text":selectOptionhtml,
              "Answer": textarea,
            };
          }
          else{
            const markerData = {
              "Point": index+1,
              "Latitude": markerPosition.lat,
              "Longitude": markerPosition.lng,
              "Address": formattedAddress,
              "Select_Machine": selectOption,
              "Select_Text":selectOptionhtml,
              "Answer": textarea,
            };
            markerDataArray[element].push(markerData);            
          }
          showMarkerData(element);
          // Close the info window
          infowindows[element][index].close();
          isInfoWindowOpen[element] = false;
        } else {
          console.error("Marker not found.");
        }
      }
      window.saveMarker = saveMarker;

      function deleteMarker(index,element) {
        if (index >= 0 && index < markers[element].length) {
          // Remove the marker from the map
          markers[element][index].setMap(null);
    
          // Remove the marker and infowindow from their respective arrays
          markers[element].splice(index, 1);
          infowindows[element][index].close();
          infowindows[element].splice(index, 1);
    
          // Remove the corresponding data from markerDataArray
          markerDataArray[element].splice(index, 1);
    
          // Update the labels of the remaining markers
          updateMarkerLabels(element);
          showMarkerData(element);
          isInfoWindowOpen[element] = false;
        } else {
          console.error("Marker not found.");
        }
      }
      window.deleteMarker = deleteMarker;

      function updateMarkerLabels(element) {
        // Update the labels of the remaining markers
        for (let i = 0; i < markers[element].length; i++) {
          markers[element][i].setLabel({
            text: (i).toString(), // Update label text with index
            color: 'white',
            fontWeight: 'bold',
          });
        }
      }

      function getoptiionsarray(element){
        var optionsarray = [];
        var textlabel = elementdata[element].selecttitle;
        if(textlabel == null || textlabel == '') {
          textlabel = Drupal.t('Select the type of point');
        }
        var selectoptions = elementdata[element].selectoption;
        optionsarray.push('<option value="">'+textlabel+'</option>');
        if($.isArray(selectoptions)) {
          selectoptions.forEach(element => {
            optionsarray.push('<option value="'+element[0]+'">'+Drupal.t($.trim(element[1]))+'</option>');        
          });  
        }
        return optionsarray;
      }
      function showMarkerData(element){
        let myJsonString;
        if(Object.values(markerDataArray[element]).length>0){
          myJsonString = JSON.stringify(Object.values(markerDataArray[element]), null, '\t');
        }
        $('#'+element+'_latlonclassresult').val(myJsonString);
      }
      Object.keys(elementdata).forEach(element => {
        google.maps.event.addListenerOnce(window, 'load', initMap(element));    
      });
//      google.maps.event.addDomListener(window, 'load', initMap);    
    },
  };
  $(document).ready(function(){
    $('#edit-show-map').hide();
    if($('iframe#reportsIframe').length>0){
      $('#edit-show-map').show();      
    }
  });
})(jQuery, Drupal, drupalSettings);
